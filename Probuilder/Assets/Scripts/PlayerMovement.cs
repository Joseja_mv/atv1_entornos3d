﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{

    public Rigidbody rigid;
    public float mouseSensitivity;
    public float moveSpeed;
    public float runningSpeed;
    public float jumpSpeed;
    public float jumpHeight;
    public float crouchPlayer;
    public float fracJourney;
    public bool isGrounded = true;
    public bool isRunning = false;
    public bool isCrouched = false;
    public Camera playerCam;
    public GameObject Player;
    public Animator anim;
    
    
    private void Start()
    {

        
    }
    // Update is called once per frame
    void FixedUpdate()
    {
        //Crouch();

        if (Input.GetKey(KeyCode.LeftShift) && !Input.GetKey(KeyCode.S)) //&& isGrounded
        {
            isRunning = true;
            MovementPlayerFunction(runningSpeed);
            anim.SetBool("corriendo", true);
            anim.SetBool("andando", false);
        }
        else
        {
            isRunning = false;
            if (moveSpeed == 0f)
            {
                Debug.Log("Quieto");
                anim.SetBool("idle", true);
                anim.SetBool("andando", false);
            }
            else
            {
                MovementPlayerFunction(moveSpeed);
                anim.SetBool("corriendo", false);
                anim.SetBool("andando", true);
            }
                              
        }
        
        if (Input.GetKey(KeyCode.Space) && isGrounded)
        {

            isGrounded = false;
            MovementPlayerFunction(jumpSpeed);
            Debug.Log("Entra");
            rigid.AddForce(Vector3.up * jumpHeight);
        }               //Jump
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.tag == "suelo")
        {
            isGrounded = true;
            Debug.Log("isgrounded = true");
        }
    }               //isGrounded = true

    public void MovementPlayerFunction(float modificador)
    {

        rigid.MovePosition(transform.position + (transform.forward * Input.GetAxis("Vertical") * modificador) + (transform.right * Input.GetAxis("Horizontal") * modificador));
        
    }

    /*public void Crouch()
    {
        if (Input.GetKey(KeyCode.LeftControl))
        {
            //Player.transform.localScale.y = new Vector3(0f,1.3f,0f);
        }
        else
        {
            
        }
    }*/
   
}
